<?php
/*
Plugin Name:  Product API checker
Plugin URI:   https://www.planviewer.nl/
Description:  Een tester voor de Product API van Planviewer
Version:      1.1.0
Author:       planviewer.nl
Author URI:   https://www.planviewer.nl/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  pac
*/
defined( 'ABSPATH' ) or die( '' );

function show_linter(){
    ?>
    <style>
        .tooltip-inner {
            max-width: 600px;
            width: 600px;
            text-align: left;
        }
    </style>
    <script>
        jQuery(function () {
            jQuery('[data-toggle="tooltip"]').tooltip({ boundary: 'window' })
        })
    </script>
    <div class="row">
        <div class="col-md-6">
            <p>
                <small>
                    <h4>Voorbeeld data</h4>
                    <pre>Stationsstraat 27, 9693 AA<br>Stationsweg 2, 9953 RG<br>Stationsplein 4, 3771 ES<br>Oud Vellerseweg 10, 3772 PA<br>Aan de Drie Heren 4, 6191 BH<br></pre>
                </small>
            </p>
        <form action="" method="post">
            <h3>Zoek op adres en postcode</h3>
            <textarea name="input_data_adres" id="inputdata" cols="50" rows="10"><?php echo ( isset($_POST['input_data_adres']) ? $_POST['input_data_adres'] : "Straatnaam 1234a, 1234 DC" ) ?></textarea><br>
            <p><input id="send" class="btn btn-primary" type="submit" value="Lint" /></p>
        </form>
        </div>
        <div class="col-md-6">
            <p>
                <small>
                    <h4>Voorbeeld data</h4>
                    <pre>AHM01 M 508<br>AHM01 M 4501<br>AHM01 P 531<br>WDV01 C 4481<br>MDT01 C 6883<br></pre>
                </small>
            </p>
            <form action="" method="post">
                <h3>Zoek op kadastrale sleutel</h3>
                <textarea name="input_data_kadsleutel" id="inputdata" cols="50" rows="10"><?php echo ( isset($_POST['input_data_kadsleutel']) ? $_POST['input_data_kadsleutel'] : "NMG00 A 211" ) ?></textarea><br>
                <p><input id="send" class="btn btn-primary" type="submit" value="Lint" /></p>
            </form>
        </div>
    </div>

    <?php

    if (isset($_POST['input_data_adres'])){

        echo "<table class='table'><thead><tr><th>Adres</th><th>Huisnummer</th><th>Postcode</th><th>Resultaat (score van hoog naar laag)</th></tr></thead>";
        $set = [];

        $addressElements = preg_split('/\r\n|[\r\n]/', $_POST['input_data_adres']);
        foreach ( $addressElements as $key => $value ){
            $address = [];

            $address = preg_split('/,\s|,/',$value);

            $regex = "/\d*[\-]?\d*.$/";
            if ( preg_match( $regex,$address[0],$result ) ){
                $set[$key]['hnr'] = $result[0];
            }

            $set[$key]['adres'] = str_replace(" " . $set[$key]['hnr'],"", $address[0]);
            $set[$key]['postcode'] = $address[1];

        }

        foreach ($set as $key => $value){
            $args = [];
            $args['hnr'] = $value['hnr'];
            $args['postcode'] = $value['postcode'];
            $args['toevoeging'] = '';

            $connection = new Pvapidm_Public('pvapidm', '1.0.0');
            $connection->searchAddress($args);

            echo "<tr><td>" . $value['adres'] . "</td><td>" . $value['hnr'] . "</td><td>" . $value['postcode'] . "</td><td>";

            if ($connection->responseObject){

                foreach($connection->responseObject as $result){
                    $postcode = str_replace(" ","", $value['postcode']);
                    if ($result->properties->huisnummer . $result->properties->huisnummertoevoeging . $result->properties->huisletter == $value['hnr'] && $postcode == $result->properties->postcode ) {
                        $resultClass = "text-success";
                    }
                    else{
                        $resultClass = "text-danger";
                    }
                    echo "<a class='" . $resultClass . "' href='?id=" . $result->uuid . "' data-toggle=\"tooltip\" data-placement=\"right\" data-html=\"true\" title='<pre class=\"text-success\">" . esc_html__(print_r($result, true)) . "</pre>'>" . $result->title . "</a> (<span style='color:green'>score: " . $result->score . "</span>)<br>";

                }

            }
            else{
                echo "<span class='text-danger'>Geen resultaat</span>";
            }

             echo "</td></tr>";
        }

        echo "</table>";
    }

    elseif (isset($_POST['input_data_kadsleutel'])){

        echo "<table class='table'><thead><tr><th>Kadastrale sleutel</th><th>Resultaat (score van hoog naar laag)</th></tr></thead>";
        $set = [];

        $addressElements = preg_split('/\r\n|[\r\n]/', $_POST['input_data_kadsleutel']);
        foreach ( $addressElements as $key => $value ){
            $address = [];

            $address = preg_split('/\s/',$value);

            $set[$key]['gemeentecode'] = $address[0];
            $set[$key]['sectie'] = $address[1];
            $set[$key]['perceelnummer'] = $address[2];

        }

        foreach ($set as $key => $value){
            $args = [];
            $args['gemeentecode'] = $value['gemeentecode'];
            $args['sectie'] = $value['sectie'];
            $args['perceelnummer'] = $value['perceelnummer'];

            $connection = new Pvapidm_Public('pvapidm', '1.0.0');
            $connection->searchLot($args);

            echo "<tr><td>" . $value['gemeentecode'] . " " . $value['sectie'] . " " . $value['perceelnummer'] . "</td><td>";

            if ($connection->responseObject){

                foreach($connection->responseObject as $result){
                    if ($result->properties->gemeentecode == $args['gemeentecode'] && $result->properties->sectie == $args['sectie'] && $result->properties->perceelnummer == $args['perceelnummer'] ){
                        $resultClass = "text-success";
                    }
                    else{
                        $resultClass = "text-danger";
                    }
                    echo "<a class='" . $resultClass . "' href='?id=" . $result->uuid . "' data-toggle=\"tooltip\" data-placement=\"right\" data-html=\"true\" title='<pre class=\"text-success\">" . esc_html__(print_r($result, true)) . "</pre>'>" . $result->title . "</a> (<span style='color:green'>score: " . $result->score . "</span>)<br>";
                }

            }
            else{
                echo "<span class='text-danger'>Geen resultaat</span>";
            }

            echo "</td></tr>";
        }

        echo "</table>";
    }
    elseif (isset($_GET['id'])){
        $args['uuid'] = $_GET['id'];
        $args['type'] = 'order';

        $connection = new Pvapidm_Public('pvapidm', '1.0.0');

        $connection->getAddress_func($args);

        $results = $connection->responseObject;
        var_dump($results);
    }




}
add_shortcode('linter', 'show_linter');
